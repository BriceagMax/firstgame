﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Скрипт управелния персонажем
public class PlayerController : MonoBehaviour
{
    // в сцене добавляем объект Камера 
    public GameObject Player;
    // если персонаж смотрит вправо
    bool facingRight = true;
    // скорость перемещения персонажа
    public int speed = 10;
    // проверям если персонаж двигается вправо
    bool move = true;
    // проверям на какую кнопку нажали
    bool isPresedL, isPresedR;

    //Инициализация
    void Start()
    {
        // Инициализация персонажа при загрузке уровня
        Player = gameObject;
    }

    // методы Up, Down для кнопок
    public void StartMoveLeft()
    {
        isPresedL = true;
    }

    public void FinishMoveLeft()
    {
        isPresedL = false;
    }

    public void StartMoveRight()
    {
        isPresedR = true;
    }

    public void FinishMoveRight()
    {
        isPresedR = false;
    }

    //////////////////////  

    // рендеринг  сцены
    public void FixedUpdate()
    {
        // получаем размер персонажа по оси х
        float width = GetComponent<SpriteRenderer>().bounds.size.x;

        // если нажата левая кнопка
        if (isPresedL)
        {
            // двигаемся влево
            Player.transform.position -= Player.transform.right * speed * Time.deltaTime;
            move = false;
        }

        // еслин нажата правая кнопка
        if (isPresedR)
        {
            //двигаемся вправо
            Player.transform.position += Player.transform.right * speed * Time.deltaTime;
            move = true;
        }

        //Если двигаем персонаж вправо, а он смотрит влево то переворачиваем
        if (move && !facingRight)
        {
            facingRight = true;
            Vector3 Scale = transform.localScale;
            Scale.x *= -1;
            transform.localScale = Scale;
            transform.position += new Vector3(width, 0);
        }
        //Если двигаем персонаж влево, а он смотрит вправо то переворачиваем
        if (!move && facingRight)
        {
            facingRight = false;
            Vector3 Scale = transform.localScale;
            Scale.x *= -1;
            transform.localScale = Scale;
            transform.position -= new Vector3(width, 0);
        }
    }
}