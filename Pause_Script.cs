﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Скрипт паузы
public class Pause_Script : MonoBehaviour {

   //Получаем из сцены объекты: панель меню, кнопка паузы, кнопки управления
    public GameObject panelMenu, pauseButton, buttons;

	// ставим игру на паузу
	public void StartPause()
    {
        // панель меню доступна, кнопка паузы и кнопки управления не доступны
        panelMenu.SetActive(true);
        pauseButton.SetActive(false);
        buttons.SetActive(false);

        // останавливаем время
        Time.timeScale = 0;
    }

    // запускаем ишру после паузы
    public void FinishPause()
    {
        // панель меню недоступна, кнопка паузы и кнопки управления становятся доступными
        panelMenu.SetActive(false);
        pauseButton.SetActive(true);
        buttons.SetActive(true);

        // запускаем время
        Time.timeScale = 1;
    }

    // метод возврата к главному меню
    public void MainMenu()
    {
        // отключаем паузу
        Time.timeScale = 1;
        string sceneName = "mainMenu";
        // загружаем сцену Меню
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single); 
    }

    // Выход из игры
    public void Exit()
    {
        Application.Quit();
    }



  
}
