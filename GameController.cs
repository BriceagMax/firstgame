﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Главный скрипт управления игровой механикой

public class GameController : MonoBehaviour {

    // в сцене добавляем объекты Камера 
    public Camera cam;
    // массив префабов для спауна 
    public GameObject[] fruits; 
    // ширина экрана
    private float maxWidth;   
    // таймер
    public float timeLeft;
    // отображение таймера
    public Text timerText;
    // вывод текста о проигыше
    public Text gameoverText;
    // панель проигрыша, кнопка рестарта уровня, объект игрок, панель очков, кнопки управления
    public GameObject gameoverPanel, restartBtn, play, scorePanel, buttons;


    // То что должно появится при загрузке уровня
    void Start () {
        // Проверяем если камера в сцене, если нет то добавляем
        if ( cam == null)        
        {
            cam = Camera.main;
        }
        // Получаем верхний угол экрана
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0.0f);
        // Синхронизируем этот угол с камерой
        Vector3 target = cam.ScreenToWorldPoint(upperCorner);
        // получаем размер по оси х, префаба для спауна 
        float fruitWidth = fruits[0].GetComponent<Renderer>().bounds.extents.x;
        // Получаем диапазон по оси х, на котором может появится фрукт
        maxWidth = target.x - fruitWidth;  
        // запускам спаун
        StartCoroutine(Spawn()); 
        //Задаем значение таймеру
        UpdateText();

    }

    // покадровое обновление
    public void FixedUpdate()
    {
       // включаем отсчет таймера от реального времени
        timeLeft -= Time.deltaTime;
        // если отсчет закончился, таймер равен 0, чтобы не было -1 и т.д.
        if(timeLeft < 0)
        {
            timeLeft = 0;

        }
        //обновляем таймер
        UpdateText();
        
    }
    //Счетчик спауна
    IEnumerator Spawn()
    {
        //Время ожидания перед началом спауна
        yield return new WaitForSeconds(1.0f);  
        // пока таймер не дошел до нуля
        while (timeLeft > 0 )                           
        {
            // создаем объект которому будут рандомно присваиватся префабы из нашего массива
            GameObject fruit = fruits[Random.Range(0, fruits.Length)];
            //Задаем область спауна фруктов по ширине экрана
            Vector3 spawnPosition = new Vector3(    
                Random.Range (-maxWidth, maxWidth ),
                transform.position.y,
                0.0f);
            // запрещаем возможность вращения префаба, она равна 0
            Quaternion spawnRotation = Quaternion.identity;  
            // запускаем спаун объектов( задаем занчение объекта, его возможную позицию и вращение)
            Instantiate(fruit, spawnPosition, spawnRotation);
            //Время появления объекта в сцене , от и до в секундах
            yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));  
        }
        //Когда таймер дошел до нуля
        ///////////////////////
        // задержка пол секунды
        yield return new WaitForSeconds(0.5f);
        // отображение панели проигрыша
        gameoverPanel.SetActive(true);
        // игрок не активен
        play.SetActive(false);
        // панель очков активна
        scorePanel.SetActive(true);
        // кнопки управления не активны
        buttons.SetActive(false);
        // задержка 1 секунда
        yield return new WaitForSeconds(1.0f);
        // кнопка рестарта активна
        restartBtn.SetActive(true);
    }

    // метод обновления таймера
    public void UpdateText()
    {
        timerText.text = "Time Left:\n" + Mathf.RoundToInt(timeLeft);
    }
}
