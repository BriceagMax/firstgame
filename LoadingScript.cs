﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Скрипт загрузки уровня
public class LoadingScript : MonoBehaviour
{
    // проверяем если сцена загружена
    private bool loadScene = false;
    //  название сцены
    public string sceneName;
    // текст загрузки
    public Text loadingText;
    // слайдер загрузки
    public Slider loadingSlider;

	// Инициализация при старте
	void Start ()
    {
        // при старте слайдер не активен
        loadingSlider.gameObject.SetActive(false);
	}
	
	// Покадровые изменения
	void Update ()
    {  
        //если сцена не загружена
        if (!loadScene)
        {
            // позволяем загрузить сцену
            loadScene = true; 
            // точ то обображается в слайдере
            loadingText.text = "Loading...";
            // запускаем перечисление LoadNewScene
            StartCoroutine(LoadNewScene(sceneName));
        }
	}

    //перечисление LoadNewScene
    IEnumerator LoadNewScene(string sceneName)
    {
        //yield return new WaitForSeconds(2.0f);
        //делаем слайдер активным
        loadingSlider.gameObject.SetActive(true);
        // синхронизируем с загрузкой сцены
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
        // гоотовим оператора async к синхронизации
        async.allowSceneActivation = false;
        
        //пока синхронизация не завершена
        while (!async.isDone)
        {
            // записываем в переменную прогресс прогресс синхронизации
            float progress = Mathf.Clamp01(async.progress / 0.1f);
            //передаем значение прогресса на слайдер
            loadingSlider.value = progress;
            // визуализируем прогресс
            loadingText.text = progress * 100f + "%"; 
            //завершаем синхронизацию
            async.allowSceneActivation = true;

            //loadingSlider.value = async.progress;
            //    if(async.progress == 0.9f)
            //{
            //    yield return new WaitForSeconds(1.0f);
            //    loadingSlider.value = 1f;
            //    async.allowSceneActivation = true;
            //}


            // перечисление ничего не возвращает
            yield return null;
        }
    }
}
