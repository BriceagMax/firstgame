﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Скрипт главного меню
public class MainMenu : MonoBehaviour
{
    // в сцене добавляем объекты кнопок и контейнеров
    public GameObject skinButton;
    public GameObject levelButton;
    public GameObject levelContainer;
    public GameObject shopContainer;

    // перемещение камеры
    private Transform cameraTransform;
    private Transform cameraLookAt;

    // Инициализация
    private void Start()
    {
        // получаем координаты камеры
        cameraTransform = Camera.main.transform;

        // загружаем  эскизы уровней в массив thumbnails
        Sprite[] thumbnails = Resources.LoadAll<Sprite>("Level");

        foreach(Sprite thumbnail in thumbnails)
        {
            // получаем объект контейнер
            GameObject container = Instantiate(levelButton) as GameObject;
            //заполняем контейнер эскизами уровней
            container.GetComponent<Image>().sprite = thumbnail;
            // уточняем что контейнер является родительским для эскизов
            container.transform.SetParent(levelContainer.transform, false);

            // имя эскиза должно соответствовать имени уровня
            string sceneName = thumbnail.name;

            // создаем евент, на клик по эскизу загружаем уровень
            container.GetComponent<Button>().onClick.AddListener(() => LoadLevel(sceneName));
        }
        // загружаем  эскизы скинов в массив thumbnails
        Sprite[] skins = Resources.LoadAll<Sprite>("Shop");

        foreach (Sprite skin in skins)
        {
            // получаем объект контейнер
            GameObject container = Instantiate(skinButton) as GameObject;
            //заполняем контейнер скинами персонажа
            container.GetComponent<Image>().sprite = skin;
            // уточняем что контейнер является родительским для скинов
            container.transform.SetParent(shopContainer.transform, false);
            // имя скина должно соответствовать имени префаба
            string skinName = skin.name;
            // создаем евент, на клик по скину загружаем новый префаб
            container.GetComponent<Button>().onClick.AddListener(() => SkinChange(skinName));
        }
    }

    // рендер сцены
    private void Update()
    {
        // изменение положения камеры по клику
        if(cameraLookAt != null)
        {
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, 
                cameraLookAt.rotation, 2 * Time.deltaTime);
        }
    }

    // загружаем уровент из скина
    private void LoadLevel(string sceneName)
    {
        //Debug.Log(scenName); 
        SceneManager.LoadScene(sceneName);
    }
    // меняем скин префаба
    private void SkinChange(string skinName)
    {
        Debug.Log(skinName + "approved");
    }
    //изменяем положение камеры
    public void LookAtMenu(Transform menuTransform)
    {
        Debug.Log(menuTransform);
        cameraLookAt = menuTransform;
    }
}
