﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Скрипт рамок экрана
public class BorderController : MonoBehaviour {

    // рамки экрана
    public float leftBorder;
    public float rightBorder;
    public float topBorder;
    public float bottomBorder;
    
    // инициализация
    void Start () {
        Vector3 pos = transform.position;
        float distance = Vector3.Distance(pos, Camera.main.transform.position);
        leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).x;
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance)).x;
        topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance)).y;
        bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance)).y;
    }
	
	// обновление
	void Update () {
        Vector3 pos = transform.position;
        transform.position = new Vector3(Mathf.Clamp(pos.x, leftBorder, rightBorder), 
            Mathf.Clamp(pos.y, bottomBorder, topBorder), pos.z );
	}
}
