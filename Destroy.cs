﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Скрипт в котором уничтожаем объекты при столкновении с триггером
//а также записываем очки и обновляем таймер
public class Destroy : MonoBehaviour {

    //Текстовый вывод очков
    public Text scoreText;      
    public Text scoreTextFinal;
    public Text appleText;
    public Text bananaText;
    public Text pineText;
    public Text appleTextFinish;
    public Text bananaTextFinish;
    public Text pineTextFinish;

    //Очки которые добавляются при коллизии
    public int fruitValue;

    //Перменные для FinalScore
    private int score, appleScore, bananaScore, pineScore;

    //Получаем объект timer из класса GameController
    public GameController game;

    //Инициализация при запуске сцены
    void Start()
    {
        score = 0;
        UpdateScore();

    }

    // Метод которые вычисляет столкнулись ли объекты
    private void OnTriggerEnter2D(Collider2D other)
    {
        // При столкновении объектов удаляем объект без коллизии 
        Destroy(other.gameObject);   
        
        //Добавляем или отнимаем, очки и время в зависимости от тега объекта
        if (other.tag == "Banana")
        {
            bananaScore += fruitValue;
            score += fruitValue;
            game.timeLeft += 3.0f;
        }
        else if (other.tag == "Apple")
        {
            appleScore += fruitValue;
            score += fruitValue;
            game.timeLeft += 1.0f;
        }
        else if (other.tag == "PineApple")
        {
            pineScore += fruitValue;
            score += fruitValue * 2;
            game.timeLeft += 10.0f;
        }
        else if(other.tag == "Burger")
        {
            score -= fruitValue * 2;
                game.timeLeft -= 3.0f;
        }
        else if (other.tag == "HotDog")
        {
            score -= fruitValue * 3;
            game.timeLeft -= 5.0f;
        }

        UpdateScore();
        //Debug.Log(game.timeLeft);
    }
    //Метод который выводит полученные результаты на экран
    void UpdateScore()
    {
        scoreText.text = "Score:\n" + score;
        scoreTextFinal.text += score.ToString();
        appleText.text = "x " + appleScore;
        appleTextFinish.text = "x " + appleScore;
        bananaText.text = "x " + bananaScore;
        bananaTextFinish.text = "x " + bananaScore;
        pineText.text = "x " + pineScore;
        pineTextFinish.text = "x " + pineScore;
    }

}
