﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Скрипт рестарта уровня
public class Restart : MonoBehaviour {
    
	public void GameRestart()
    {
        //Загружаем сцену loadingLevel
        SceneManager.LoadScene(1);     
    }
}
