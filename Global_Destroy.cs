﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Глобальное уничтожение
public class Global_Destroy : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Если мы не словили объект то он уничтожается
        Destroy(other.gameObject);      
    }

}
